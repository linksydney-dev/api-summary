# LinkPos API 文档说明
    api url:
    生产环境： https://api.linkaumall.com
    开发环境： https://api.linkpos.com.au

### 登陆获取Token

`POST` /account/login/username

* 头部信息
  * Content-Type: application/json
  
* Body参数
```json
    {
      "username":"your mobile number registered with LINK",
      "password":"your password",
      "principle":"MOBILE"
    }
```

* Sample Response 

```json
  {
  "accessToken": "your Authorization token to be used for all other api calls"
  }
```

### 查找全部店铺
[Swaggert 链接](https://api.linkpos.com.au/store/swagger-ui.html?urls.primaryName=version1#/store-controller-v-1)

`GET` /store/v1/profile

* 头部信息
    * Content-Type: application/json
    * Authorization: {accessToken}
* 路径参数

* 查询参数
    - pageSize(必填)： specify number of returned items on each page
    - pageIdx(选填)： specify the index of the returned page for result
    - includeDeleted(选填): Admin only, please ignore for third party usage

Sample request

```
https://api.linkpos.com.au/store/v1/profile
```

返回数据: 菜单数据
```json
{
  "records": [
    {
      "address": {
        "address1": "string",
        "address2": "string",
        "city": "string",
        "lat": 0,
        "lng": 0,
        "postCode": "string",
        "state": "string"
      },
      "id": "string",
      "logoImgUrl": "string",
      "name": "string",
      "phone": "string"
    },
    ...
  ]
}
```

### 从店铺查找菜单

[Swaggert 链接](https://api.linkpos.com.au/store/swagger-ui.html?urls.primaryName=version1#/store-controller-v-1)

`GET` /store/v1/profile/{storeId}/menu

* 头部信息
    - Content-Type: application/json
    - Authorization:{accessToken}
* 路径参数
    - storeId(必填): store ID  

* 查询参数
    - language(选填): supported - ZH_CN, EN_US

Sample request

```
https://api.linkpos.com.au/store/v1/profile/xxxvafsaasfsafasf/menu?language=ZH_CN
```

返回数据: 菜单数据
```json
{
  "categories": [
    {
      "id": "string",
      "name": "string",
      "products": [
        {
          "appScope": [
            "WXAPP"
          ],
          "availableTimes": [
            {
              "daysOfWeek": [
                0
              ],
              "end": "string",
              "start": "string"
            }
          ],
          "config": {
            "acceptDelivery": "NEVER",
            "acceptDineIn": "NEVER",
            "acceptLinkDelivery": "NEVER",
            "acceptStock": true,
            "acceptTakeaway": "NEVER",
            "dynamicPrice": true,
            "measureType": "KG"
          },
          "description": "string",
          "id": "string",
          "imgUrl": "string",
          "name": "string",
          "options": [
            {
              "code": "string",
              "description": "string",
              "max": 0,
              "min": 0,
              "name": "string",
              "optionValues": [
                {
                  "description": "string",
                  "max": 0,
                  "min": 0,
                  "name": "string",
                  "price": 0,
                  "sortOrder": 0,
                  "stock": 0,
                  "translations": [
                    {
                      "description": "string",
                      "languageCode": "ZH_CN",
                      "languageId": "string",
                      "name": "string"
                    }
                  ],
                  "value": "string"
                }
              ],
              "sortOrder": 0,
              "translations": [
                {
                  "description": "string",
                  "languageCode": "ZH_CN",
                  "languageId": "string",
                  "name": "string"
                }
              ]
            }
          ],
          "originalPrice": 0,
          "price": 0,
          "printZone": {
            "created": {
              "date": 0,
              "day": 0,
              "hours": 0,
              "minutes": 0,
              "month": 0,
              "nanos": 0,
              "seconds": 0,
              "time": 0,
              "timezoneOffset": 0,
              "year": 0
            },
            "error": {
              "code": "string",
              "reason": "string"
            },
            "id": "string",
            "metaData": {
              "assemblyPayment": {
                "merchantReceipt": "string"
              },
              "wxRefund": {
                "refundFee": "string",
                "totalFee": "string",
                "wxRefundId": "string"
              }
            },
            "note": "string",
            "operator": "string",
            "refund": {
              "amount": 0,
              "refundId": "string",
              "status": "INITIATED"
            },
            "status": "INITIATED"
          },
          "reward": 0,
          "sortOrder": 0,
          "stock": 0,
          "translations": [
            {
              "description": "string",
              "languageCode": "ZH_CN",
              "languageId": "string",
              "name": "string"
            }
          ],
          "type": "BASIC",  // BASIC, OPTIONAL, BUNDLE 三种类型，BASIC是最基本的菜，OPTIONAL是带选项的，BUNDLE 是多个BASIC菜的组合
          "warningStock": 0
        }
      ],
      "sortOrder": 0,
      "translations": [
        {
          "description": "string",
          "languageCode": "ZH_CN",
          "languageId": "string",
          "name": "string"
        }
      ]
    }
  ],
  "discountProducts": [
    {
      "appScope": [
        "WXAPP"
      ],
      "availableTimes": [
        {
          "daysOfWeek": [
            0
          ],
          "end": "string",
          "start": "string"
        }
      ],
      "config": {
        "acceptDelivery": "NEVER",
        "acceptDineIn": "NEVER",
        "acceptLinkDelivery": "NEVER",
        "acceptStock": true,
        "acceptTakeaway": "NEVER",
        "dynamicPrice": true,
        "measureType": "KG"
      },
      "description": "string",
      "id": "string",
      "imgUrl": "string",
      "name": "string",
      "options": [
        {
          "code": "string",
          "description": "string",
          "max": 0,
          "min": 0,
          "name": "string",
          "optionValues": [
            {
              "description": "string",
              "max": 0,
              "min": 0,
              "name": "string",
              "price": 0,
              "sortOrder": 0,
              "stock": 0,
              "translations": [
                {
                  "description": "string",
                  "languageCode": "ZH_CN",
                  "languageId": "string",
                  "name": "string"
                }
              ],
              "value": "string"
            }
          ],
          "sortOrder": 0,
          "translations": [
            {
              "description": "string",
              "languageCode": "ZH_CN",
              "languageId": "string",
              "name": "string"
            }
          ]
        }
      ],
      "originalPrice": 0,
      "price": 0,
      "printZone": {
        "created": {
          "date": 0,
          "day": 0,
          "hours": 0,
          "minutes": 0,
          "month": 0,
          "nanos": 0,
          "seconds": 0,
          "time": 0,
          "timezoneOffset": 0,
          "year": 0
        },
        "error": {
          "code": "string",
          "reason": "string"
        },
        "id": "string",
        "metaData": {
          "assemblyPayment": {
            "merchantReceipt": "string"
          },
          "wxRefund": {
            "refundFee": "string",
            "totalFee": "string",
            "wxRefundId": "string"
          }
        },
        "note": "string",
        "operator": "string",
        "refund": {
          "amount": 0,
          "refundId": "string",
          "status": "INITIATED"
        },
        "status": "INITIATED"
      },
      "reward": 0,
      "sortOrder": 0,
      "stock": 0,
      "translations": [
        {
          "description": "string",
          "languageCode": "ZH_CN",
          "languageId": "string",
          "name": "string"
        }
      ],
      "type": "BASIC",
      "warningStock": 0
    }
  ]
}
```

### 创建订单

[Swaggert 链接](https://api.linkpos.com.au/store/swagger-ui.html?urls.primaryName=version2#/store-order-controller-v-2)

`Post` /store/v2/{storeId}/order

* 头部信息
    - Content-Type: application/json
    - Authorization: {accessToken}
* 路径参数
    * storeId(必填): store ID  
* Body req(必填): 
  
```json
       OrderCreateRequestV2 {
            storeId:	string, // store ID
            type:	string, // Enum:   [DINEIN(堂食）, TAKEAWAY(自取外卖), DELIVERY（送门外卖）, BOOKING（预约订单）],
            items:	[CartItemDTO], // menu items included in the order (所点菜品)， please refer to CartItemDTO
            promotionId:	string, // promotion id (optional) 商家促销活动ID
            takeawayDetail:	TakeawayDetail, // 外卖详情
            deliveryDetail:  DeliveryDetailInCreateOrderRequest, // 送餐详情
            dineInDetail:	DineInDetail, // 堂食详情
            note:	string, // 备注
            diners:	integer, // 就餐人数 （堂食only）
            orderClient:	[WXAPP, POS, LINKORDER, UNKNOWN], // 订单渠道
            surcharge:	integer, // 附加费用， 以cent为单位的整形
            deliveryProviderAdjustment:	integer, // 送餐服务商的价格预留，如对悉尼送餐的订单上浮100cent
            app:	[LINKEAT, LINKEDUCATION, LINKFOOD],
            formId:	string, // 微信小程序订单发送消息的模版ID optional
            memberId:	string, // 商家会员ID optional
        }
        
        CartItemDTO{
            productId	string($uuid) // 菜品id, store/menu 的api可以拿回
            price	integer($int32) // 价格， 以cent为单位
            options	[...] // 用户选择的菜品选项数据，如“辣度”，“份量”等，只针对"OPTIONAL, BUNDLE"类型的菜品
            quantity	integer($int32) // 数量
            measuredValue	number($double) //  称重的重量，选择填写, 单位取决与菜品里的“measureType”
            note	string // 备注
        }

        TakeawayDetail {
            customerName	string // 客户姓名
            phone	string // 联系电话
            pickUpTime	Timestamp // 取餐时间
            deliveryProvider	string [HARKHARK, EASI, UBEREATS, FOODORA, MENULOG, DELIVEROO, ZSAZ, OTHER]
        }

        DeliveryDetailInCreateOrderRequest
        {
            name	string // 姓名
            phone	string // 联系电话
            expectArriveTime	Timestamp // 预计到达时间
            address	AddressInOrderCreateRequest // 地址信息
        }

        DineInDetail {
            phone	string // 电话
            tableId	string($uuid) // 桌号
            tableValidationCode	string($uuid) //桌号验证码
        }
```


Sample request

```
{
    "storeId": "string",
    "type": "DINEIN",
    "items": [
        {
          "productId": "9ae2c847-764a-43ce-90fe-8aa4c380c476",
          "price": 4499,
          "quantity": 1,
          "options": [
            {
              "code": "01",
              "optionValues": [
                {
                  "value": "cakex3",
                  "quantity": 1,
                }
              ],
            }
          ],
          "note": "",
          "measuredValue": null
        }
      ],
    "promotionId": "string",
    "takeawayDetail": {
        "customerName": "string",
        "phone": "string",
        "pickUpTime": {
        "date": 0,
        "day": 0,
        "hours": 0,
        "minutes": 0,
        "month": 0,
        "nanos": 0,
        "seconds": 0,
        "time": 0,
        "timezoneOffset": 0,
        "year": 0
        },
        "deliveryProvider": "HARKHARK"
    },
    "deliveryDetail": {
        "name": "string",
        "phone": "string",
        "expectArriveTime": {
        "date": 0,
        "day": 0,
        "hours": 0,
        "minutes": 0,
        "month": 0,
        "nanos": 0,
        "seconds": 0,
        "time": 0,
        "timezoneOffset": 0,
        "year": 0
        },
        "address": {
        "address1": "string",
        "address2": "string",
        "city": "string",
        "state": "string",
        "postCode": "string"
        }
    },
    "dineInDetail": {
        "phone": "string",
        "tableId": "string",
        "tableValidationCode": "string"
    },
    "note": "string",
    "diners": 0,
    "orderClient": "WXAPP",
    "surcharge": 0,
    "deliveryProviderAdjustment": 0,
    "app": "LINKEAT",
    "formId": "string",
    "memberId": "string"
    }
 
```

返回数据: 创建成功
200
```json
{
  "amount": 0,
  "booking": {
    "clientContactNo": "string",
    "clientId": "string",
    "clientName": "string",
    "clientNote": "string",
    "events": [
      {
        "reasonMessage": "string",
        "type": "ACCEPT",
        "vendorNote": "string"
      }
    ],
    "number": "string",
    "numberOfPeople": 0,
    "orderId": "string",
    "refNumber": "string",
    "startTime": {
      "date": 0,
      "day": 0,
      "hours": 0,
      "minutes": 0,
      "month": 0,
      "nanos": 0,
      "seconds": 0,
      "time": 0,
      "timezoneOffset": 0,
      "year": 0
    },
    "status": "UNPROCESSED",
    "store": {
      "address": {
        "address1": "string",
        "address2": "string",
        "city": "string",
        "lat": 0,
        "lng": 0,
        "postCode": "string",
        "state": "string"
      },
      "id": "string",
      "logoImgUrl": "string",
      "name": "string",
      "phone": "string"
    },
    "submittedTime": {
      "date": 0,
      "day": 0,
      "hours": 0,
      "minutes": 0,
      "month": 0,
      "nanos": 0,
      "seconds": 0,
      "time": 0,
      "timezoneOffset": 0,
      "year": 0
    },
    "table": {
      "active": true,
      "capacity": 0,
      "code": "string",
      "created": {
        "date": 0,
        "day": 0,
        "hours": 0,
        "minutes": 0,
        "month": 0,
        "nanos": 0,
        "seconds": 0,
        "time": 0,
        "timezoneOffset": 0,
        "year": 0
      },
      "deleted": true,
      "id": "string",
      "modifiedAt": {
        "date": 0,
        "day": 0,
        "hours": 0,
        "minutes": 0,
        "month": 0,
        "nanos": 0,
        "seconds": 0,
        "time": 0,
        "timezoneOffset": 0,
        "year": 0
      },
      "sortOrder": 0,
      "validationCode": "string"
    },
    "uuid": "string"
  },
  "clientNote": "string",
  "createdAt": {
    "date": 0,
    "day": 0,
    "hours": 0,
    "minutes": 0,
    "month": 0,
    "nanos": 0,
    "seconds": 0,
    "time": 0,
    "timezoneOffset": 0,
    "year": 0
  },
  "customDiscount": 0,
  "deliveryDetail": {
    "address": {
      "address1": "string",
      "address2": "string",
      "city": "string",
      "lat": 0,
      "lng": 0,
      "postCode": "string",
      "state": "string"
    },
    "expectArriveTime": {
      "date": 0,
      "day": 0,
      "hours": 0,
      "minutes": 0,
      "month": 0,
      "nanos": 0,
      "seconds": 0,
      "time": 0,
      "timezoneOffset": 0,
      "year": 0
    },
    "name": "string",
    "phone": "string"
  },
  "deliveryFee": 0,
  "deliveryProviderAdjustment": 0,
  "gstAmount": 0,
  "items": [
    {
      "measureType": "KG",
      "measuredValue": 0,
      "note": "string",
      "options": [
        {
          "code": "string",
          "extraCharge": 0,
          "max": 0,
          "min": 0,
          "name": "string",
          "selections": [
            {
              "name": "string",
              "price": 0,
              "quantity": 0,
              "translations": [
                {
                  "description": "string",
                  "languageCode": "ZH_CN",
                  "languageId": "string",
                  "name": "string"
                }
              ],
              "value": "string"
            }
          ],
          "translations": [
            {
              "description": "string",
              "languageCode": "ZH_CN",
              "languageId": "string",
              "name": "string"
            }
          ]
        }
      ],
      "printZone": {
        "created": {
          "date": 0,
          "day": 0,
          "hours": 0,
          "minutes": 0,
          "month": 0,
          "nanos": 0,
          "seconds": 0,
          "time": 0,
          "timezoneOffset": 0,
          "year": 0
        },
        "error": {
          "code": "string",
          "reason": "string"
        },
        "id": "string",
        "metaData": {
          "assemblyPayment": {
            "merchantReceipt": "string"
          },
          "wxRefund": {
            "refundFee": "string",
            "totalFee": "string",
            "wxRefundId": "string"
          }
        },
        "note": "string",
        "operator": "string",
        "refund": {
          "amount": 0,
          "refundId": "string",
          "status": "INITIATED"
        },
        "status": "INITIATED"
      },
      "product": {
        "categoryId": "string",
        "discount": {
          "dateEnd": {
            "date": 0,
            "day": 0,
            "hours": 0,
            "minutes": 0,
            "month": 0,
            "nanos": 0,
            "seconds": 0,
            "time": 0,
            "timezoneOffset": 0,
            "year": 0
          },
          "dateStart": {
            "date": 0,
            "day": 0,
            "hours": 0,
            "minutes": 0,
            "month": 0,
            "nanos": 0,
            "seconds": 0,
            "time": 0,
            "timezoneOffset": 0,
            "year": 0
          },
          "discountId": "string",
          "price": 0
        },
        "imgUrl": "string",
        "measureType": "KG",
        "name": "string",
        "price": 0,
        "reward": 0,
        "translations": [
          {
            "description": "string",
            "languageCode": "ZH_CN",
            "languageId": "string",
            "name": "string"
          }
        ],
        "type": "BASIC",
        "uuid": "string"
      },
      "quantity": 0,
      "status": "PROCESSING",
      "unitPrice": 0
    }
  ],
  "member": {
    "memberId": "string",
    "mobile": "string",
    "nickName": "string",
    "reward": 0
  },
  "numOfPeople": 0,
  "orderNumber": 0,
  "paymentStatus": "UNPAID",
  "printingStatus": "NORMAL",
  "promotion": {
    "description": "string",
    "discount": {
      "type": "PERCENTAGE",
      "value": 0
    },
    "gift": [
      {
        "imgUrl": "string",
        "price": 0,
        "productId": "string",
        "productName": "string",
        "quantity": 0
      }
    ],
    "name": "string"
  },
  "promotionDiscount": 0,
  "status": "PENDING",
  "store": {
    "address": {
      "address1": "string",
      "address2": "string",
      "city": "string",
      "lat": 0,
      "lng": 0,
      "postCode": "string",
      "state": "string"
    },
    "id": "string",
    "logoImgUrl": "string",
    "name": "string",
    "phone": "string"
  },
  "subtotal": 0,
  "surcharge": 0,
  "tableCode": "string",
  "takeAwayDetail": {
    "address": {
      "address1": "string",
      "address2": "string",
      "city": "string",
      "lat": 0,
      "lng": 0,
      "postCode": "string",
      "state": "string"
    },
    "customerName": "string",
    "deliveryProvider": "HARKHARK",
    "numberOfPeople": 0,
    "phone": "string",
    "pickUpTime": {
      "date": 0,
      "day": 0,
      "hours": 0,
      "minutes": 0,
      "month": 0,
      "nanos": 0,
      "seconds": 0,
      "time": 0,
      "timezoneOffset": 0,
      "year": 0
    }
  },
  "totalTradeFee": 0,
  "transactions": [
    {
      "amount": 0,
      "created": {
        "date": 0,
        "day": 0,
        "hours": 0,
        "minutes": 0,
        "month": 0,
        "nanos": 0,
        "seconds": 0,
        "time": 0,
        "timezoneOffset": 0,
        "year": 0
      },
      "currency": "CNY",
      "events": [
        {
          "created": {
            "date": 0,
            "day": 0,
            "hours": 0,
            "minutes": 0,
            "month": 0,
            "nanos": 0,
            "seconds": 0,
            "time": 0,
            "timezoneOffset": 0,
            "year": 0
          },
          "error": {
            "code": "string",
            "reason": "string"
          },
          "id": "string",
          "metaData": {
            "assemblyPayment": {
              "merchantReceipt": "string"
            },
            "wxRefund": {
              "refundFee": "string",
              "totalFee": "string",
              "wxRefundId": "string"
            }
          },
          "note": "string",
          "operator": "string",
          "refund": {
            "amount": 0,
            "refundId": "string",
            "status": "INITIATED"
          },
          "status": "INITIATED"
        }
      ],
      "method": "CASH",
      "status": "INITIATED",
      "tradeFee": 0,
      "type": "NORMAL",
      "uuid": "string"
    }
  ],
  "type": "DINEIN",
  "uuid": "string"
}
```

### 优惠券API

[Swaggert 链接](https://api.linkpos.com.au/store/swagger-ui.html?urls.primaryName=beta#/voucher-controller)

#### 获取优惠券列表
`GET` /store/{storeId}/voucher

* 头部信息
  * Content-Type: application/json
  * Authorization: {accessToken}
* 路径参数
  * storeId(必填): store ID  

* 查询参数
  * pageIdx (选填): integer // default value: 0
  * pageSize  (必填): integer // default value: 0 

* Sample Response
  
```json
        {
        "pageIdx": 0,
        "pageSize": 0,
        "records": [
            {
            "availability": {
                "availableTimes": [
                {
                    "daysOfWeek": [
                    0
                    ],
                    "end": 0,
                    "start": 0
                }
                ],
                "effectiveEnd": "string",
                "effectiveStart": "string"
            },
            "claimEnd": {
                "date": 0,
                "day": 0,
                "hours": 0,
                "minutes": 0,
                "month": 0,
                "nanos": 0,
                "seconds": 0,
                "time": 0,
                "timezoneOffset": 0,
                "year": 0
            },
            "claimStart": {
                "date": 0,
                "day": 0,
                "hours": 0,
                "minutes": 0,
                "month": 0,
                "nanos": 0,
                "seconds": 0,
                "time": 0,
                "timezoneOffset": 0,
                "year": 0
            },
            "conditions": [
                {
                "type": "THRESHOLD",
                "value": "string"
                }
            ],
            "deleted": true,
            "description": "string",
            "discount": {
                "type": "PERCENTAGE",
                "value": 0
            },
            "exclusiveness": {
                "bundleProduct": true,
                "specialProduct": true
            },
            "gift": [
                {
                "imgUrl": "string",
                "price": 0,
                "productId": "string",
                "productName": "string",
                "quantity": 0
                }
            ],
            "id": "string",
            "modifiedAt": {
                "date": 0,
                "day": 0,
                "hours": 0,
                "minutes": 0,
                "month": 0,
                "nanos": 0,
                "seconds": 0,
                "time": 0,
                "timezoneOffset": 0,
                "year": 0
            },
            "name": "string",
            "priority": 0,
            "status": "DRAFT",
            "stock": 0,
            "stores": [
                {
                "id": "string",
                "name": "string"
                }
            ],
            "type": "DISCOUNT"
            }
        ],
        "totalCount": 0 }
```

#### 获取优惠券详情

`GET` /store/{storeId}/voucher/{VoucherId}

* 头部信息
  * Content-Type: application/json
  * Authorization: {accessToken}
* 路径参数
  * storeId(必填): store ID  
  * voucherId(必填): voucher ID  

* 查询参数

* Sample Response

```json
    {
    "availability": {
        "availableTimes": [
        {
            "daysOfWeek": [
            0
            ],
            "end": 0,
            "start": 0
        }
        ],
        "effectiveEnd": "string",
        "effectiveStart": "string"
    },
    "claimEnd": {
        "date": 0,
        "day": 0,
        "hours": 0,
        "minutes": 0,
        "month": 0,
        "nanos": 0,
        "seconds": 0,
        "time": 0,
        "timezoneOffset": 0,
        "year": 0
    },
    "claimStart": {
        "date": 0,
        "day": 0,
        "hours": 0,
        "minutes": 0,
        "month": 0,
        "nanos": 0,
        "seconds": 0,
        "time": 0,
        "timezoneOffset": 0,
        "year": 0
    },
    "conditions": [
        {
        "type": "THRESHOLD",
        "value": "string"
        }
    ],
    "deleted": true,
    "description": "string",
    "discount": {
        "type": "PERCENTAGE",
        "value": 0
    },
    "exclusiveness": {
        "bundleProduct": true,
        "specialProduct": true
    },
    "gift": [
        {
        "imgUrl": "string",
        "price": 0,
        "productId": "string",
        "productName": "string",
        "quantity": 0
        }
    ],
    "id": "string",
    "modifiedAt": {
        "date": 0,
        "day": 0,
        "hours": 0,
        "minutes": 0,
        "month": 0,
        "nanos": 0,
        "seconds": 0,
        "time": 0,
        "timezoneOffset": 0,
        "year": 0
    },
    "name": "string",
    "priority": 0,
    "status": "DRAFT",
    "stock": 0,
    "stores": [
        {
        "id": "string",
        "name": "string"
        }
    ],
    "type": "DISCOUNT"
    }
```

#### 领取优惠券

`PUT` /store/{storeId}/voucher/{VoucherId}/collect

* 头部信息
  * Content-Type: application/json
  * Authorization: Bearer {accessToken}
* 路径参数
  * storeId(必填): store ID  
  * voucherId(必填): voucher ID  

* Body参数
  ```json
   ```

* Sample Response
  
```json
  {}
```

#### 优惠券核销

`POST` /store/{storeId}/voucher/{voucherId}/user/{userId}/redeem

* 头部信息
  * Content-Type: application/json
  * Authorization: {accessToken}
* 路径参数
  * storeId(必填): store ID  
  * voucherId(必填): voucher ID  
  * userId(必填)： user ID

* Body参数
  

* Sample Response
  
  ```json
  {}
  ```

#### 发起支付请求

`POST` /store/v2/{storeId}/transaction

* 头部信息
  * Content-Type: application/json
  * Authorization: {accessToken}
* 路径参数
  * storeId(必填): store ID  

* Body参数
```json
    InitiateTransactionRequestV1{
        generatedTransactionId	string($uuid)
        orderId	string($uuid)
        paymentMethod [CASH, SUPERPAY, SUPERPAYMINIPROGRAM, SUPERPAYSCANORDER, ASSEMBLYPAYMENT, STRIPE, CARD, UNKNOWN, POSTPAY, WXPAY, PAYLINX, PAYLINXMINIPROGRAM, OMIPAY, OMIPAYMINIPROGRAM, OMIPAYSCANORDER, NOAHPAY ]	
        currency	[NY, AUD, USD]
        note	string
        amount	integer($int64)
        app	[LINKEAT, LINKEDUCATION, LINKFOOD]
        device [WXAPP, POS, LINKORDER, UNKNOWN]
        cardType [DEBIT, CREDIT]
        qrCode	string
        posNo	string
    }
```
* Sample Request 
  
```json
    {
        "generatedTransactionId": "string", // 
        "orderId": "string", // order id created when create order
        "paymentMethod": "CASH", // payment method
        "currency": "CNY", // CNY/AUD
        "note": "string",
        "amount": 0,
        "app": "LINKEAT",
        "device": "WXAPP",
        "cardType": "DEBIT",
        "qrCode": "string",
        "posNo": "string"
    }
```

* Sample Response
  
```json
  {}
```

#### 更新支付请求状态

`PUT` /store/v2/{storeId}/transaction/{transactionId}/overrideAsPaid

* 头部信息
  * Content-Type: application/json
  * Authorization: {accessToken}
* 路径参数
  * storeId(必填): store ID 
  * transactionId(必填)： transaction ID created when initiates transaction 
  * note (必填): string

  
